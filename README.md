# Consejos para el coloquio:

El coloquio estará basado en los temas vistos en clase, y en el trabajo práctico. 

TRAQUILOS QUE NO SE PIDE COMANDOS DE MEMORIA, NO SE PIDE CÓDIGO!!! 

Se espera que puedan:
- Demostrar su participación en el trabajo práctico, mínimamente deben saber cómo ejecutar un script. 
- Se los pondrá a prueba con interpretaciones de ejecuciones y de script, no se realizarán preguntas teóricas.


## Repasar comandos básicos, por ejemplo, mínimo: mv, cp, grep, file, ls, chmod, mkdir, ps, exit, echo, test
- Repasaría de ``ls`` las distintas salidas que tiene, como ver permisos, filtrar, formatos de salida, ocultos, etc.
- Repasaría de ``mkdir, cp, mv, etc`` como usarlos, cuando darían dar error, etc.
- Repasaría de ``grep`` como filtrar por nombre, por tipo, etc.
- Repasaría de ``chmod`` como dar permisos en octal, en aritmético, cuáles son las combinaciones útiles de permisos, etc.

## Repasar variables especiales
- Repasar $$, ¡$!, $#, $?, etc
- Repasar sus scope.
- Jueguen con funciones y que pasa cuando usan algunas de ellas

## Repasar manejo de errores
- Repasar como mostrar errores
- Repasar como indican los comandos si se ejecutaron bien o no
- Repasar como consultar la salida de un comando
- Repasar como ignorar un error

## Repasar funciones
- Repasar definición de funciones, parámetros, etc.

## Repasar operadores
- Repasaría | && & $() `` < >> >

## Repasar algunos puntos del tp 
- Repasaría comprimir, descomprimir, etc
- Enviar señales, capturar señales, etc

## Repasar AWK
- Como estructura, patrones, variables (NR, NF, etc)
- Como trabajar entradas de campos de longitud fija, longitud variable, etc.

## Repasar SED
- Repasaría como hacer sustituciones, operadores L g u etc